<?php
        
namespace Rubeus\FilaProcesso\Dominio;
use Rubeus\ORM\Persistente as Persistente;

    class AgendaProcesso extends Persistente{
        private $id = false;
        private $chave = false;
        private $processo = false;
        private $etapa = false;
        private $executado = false;
        private $resultado = false;
        private $recursivo = false;
        private $fila = false;
        private $intervaloExecucao = false;
        private $projeto = false;
        private $inicio = false;
        private $fim = false;
        private $momento = false;
        private $ativo = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getChave() {
            return $this->chave;
        }

        public function setChave($chave) {
            $this->chave = $chave;
        } 
                
        public function getProcesso() {
            return $this->processo;
        }

        public function setProcesso($processo) {
            $this->processo = $processo;
        } 
                
        public function getEtapa() {
            return $this->etapa;
        }

        public function setEtapa($etapa) {
            $this->etapa = $etapa;
        } 
                
        public function getExecutado() {
            return $this->executado;
        }

        public function setExecutado($executado) {
            $this->executado = $executado;
        } 
                
        public function getResultado() {
            return $this->resultado;
        }

        public function setResultado($resultado) {
            $this->resultado = $resultado;
        } 
                
        public function getRecursivo() {
            return $this->recursivo;
        }

        public function setRecursivo($recursivo) {
            $this->recursivo = $recursivo;
        } 
                
        public function getFila() {
            return $this->fila;
        }

        public function setFila($fila) {
            $this->fila = $fila;
        } 
                
        public function getIntervaloExecucao() {
            return $this->intervaloExecucao;
        }

        public function setIntervaloExecucao($intervaloExecucao) {
            $this->intervaloExecucao = $intervaloExecucao;
        } 
                
        public function getProjeto() {
            return $this->projeto;
        }

        public function setProjeto($projeto) {
            $this->projeto = $projeto;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
        
        public function getInicio() {
            return $this->inicio;
        }

        public function getFim() {
            return $this->fim;
        }

        public function setInicio($inicio) {
            $this->inicio = $inicio;
        }

        public function setFim($fim) {
            $this->fim = $fim;
        }

        
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        }
        
    }