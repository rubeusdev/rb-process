<?php
        
namespace Rubeus\FilaProcesso\Dominio;
use Rubeus\ORM\Persistente as Persistente;

    class ParametroProcessoAgendado extends Persistente{
        private $id = false;
        private $chave = false;
        private $valor = false;
        private $agendaProcesso = false;
        private $tipoParametroProcessoAgendado = false;
        private $momento = false;
        private $ativo = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getChave() {
            return $this->chave;
        }

        public function setChave($chave) {
            $this->chave = $chave;
        } 
                
        public function getValor() {
            return $this->valor;
        }

        public function setValor($valor) {
            $this->valor = $valor;
        } 
            
        public function getAgendaProcesso() {
            if(!$this->agendaProcesso)
                    $this->agendaProcesso = new AgendaProcesso(); 
            return $this->agendaProcesso;
        }

        public function setAgendaProcesso($agendaProcesso) {
            if($agendaProcesso instanceof AgendaProcesso)
                $this->agendaProcesso = $agendaProcesso;
            else $this->getAgendaProcesso()->setId($agendaProcesso);
        } 
            
        public function getTipoParametroProcessoAgendado() {
            if(!$this->tipoParametroProcessoAgendado)
                    $this->tipoParametroProcessoAgendado = new TipoParametroProcessoAgendado(); 
            return $this->tipoParametroProcessoAgendado;
        }

        public function setTipoParametroProcessoAgendado($tipoParametroProcessoAgendado) {
            if($tipoParametroProcessoAgendado instanceof TipoParametroProcessoAgendado)
                $this->tipoParametroProcessoAgendado = $tipoParametroProcessoAgendado;
            else $this->getTipoParametroProcessoAgendado()->setId($tipoParametroProcessoAgendado);
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        }
        
    }