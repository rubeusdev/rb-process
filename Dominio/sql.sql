
        
CREATE TABLE `agendaprocesso`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `chave` varchar(255)   ,
    `processo` varchar(255)   ,
    `etapa` varchar(255)   ,
    `executado` int   ,
    `resultado` tinyint   ,
    `respostatotvs` text   ,
    `recursivo` tinyint   ,
    `fila` tinyint   ,
    `intervaloexecucao` int   ,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `tipoparametroprocessoagendado`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `titulo` varchar(75)   ,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `parametroprocessoagendado`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `chave` varchar(75)   ,
    `valor` varchar(255)   ,
    `agendaprocesso_id` INT   ,
    INDEX `parametroprocessoagendado_fk_agendaprocesso_id_idx`(`agendaprocesso_id` ASC),
    CONSTRAINT `parametroprocessoagendado_fk_agendaprocesso_id` 
         FOREIGN KEY (`agendaprocesso_id`) REFERENCES `agendaprocesso` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `tipoparametroprocessoagendado_id` INT   ,
    INDEX `parametroprocessoagendado_fk_tipoparametroprocessoagendado_id_idx`(`tipoparametroprocessoagendado_id` ASC),
    CONSTRAINT `parametroprocessoagendado_fk_tipoparametroprocessoagendado_id` 
         FOREIGN KEY (`tipoparametroprocessoagendado_id`) REFERENCES `tipoparametroprocessoagendado` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;