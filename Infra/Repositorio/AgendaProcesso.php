<?php
namespace Rubeus\FilaProcesso\Infra\Repositorio;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class AgendaProcesso{
    
    public function consultarProcessoFila($colocarRegistroFila,$projeto=false){
        $query = Conteiner::getInstancia('Query',false);
        $query->select('id');
        $query->from('agendaprocesso','ap');
        $query->where('ap.ativo=1')->add('ifnull(ap.executado,0) < 5')->add('ap.fila=1');
        if($projeto){
            $query->addVariaveis($projeto)->where('ap.projeto = ?');
        }
        if($colocarRegistroFila){
            $query->where('ap.id not in('.implode(',',$colocarRegistroFila).')');
        }
        $query->limit(1);
//        var_dump($query->string(), $query->getVariaveis(), $query->executar('AA1',[],'id'));
        return $query->executar('AA1',[],'id');
    }
    
    public function consultar($colocarRegistroFila,$projeto=false,$fila=false){
        $subQuery = Conteiner::getInstancia('Query',false);
        $subQuery->select('*');
        $subQuery->from('agendaprocesso','ap');
        $subQuery->where('ap.ativo=1')->add('ifnull(ap.executado,0) < 5');
        if($colocarRegistroFila){
            if($fila){
                $subQuery->where('ap.fila=1');
            }else{
                $subQuery->where('ap.fila=0');
            }
            $subQuery->where('ap.id in('.implode(',',$colocarRegistroFila).')');
        }else{
            $subQuery->where('ap.fila=1');
        }
        $query = Conteiner::getInstancia('Query',false);
        if($projeto){
            $query->addVariaveis($projeto);
            $subQuery->where('ap.projeto = ?');
        }
        $subQuery->limit(10);
        
         
        $query->select('ap.id')->add('ifnull(ap.executado,0)','executado')->add('ap.processo')
                ->add('ap.etapa')->add('ppa.chave')->add('ppa.valor')->add('ppa.id','idParametro')
                ->add('ifnull(ap.recursivo,0)','recursivo');
        $query->from($subQuery,'ap');
        $query->join('parametroprocessoagendado','ppa')->on('ap.id = ppa.agendaprocesso_id');
       
	return $query->executar('EA',false,$this->estrutura());
    }
    
    private function estrutura(){
        return  [
                    'id'=>'id',
                    'executado'=>'executado',
                    'recursivo'=>'recursivo',
                    'processo'=>'processo', 
                    'etapa'=>'etapa', 
                    'paramentro'=>[
                       'idParametro' => 'id', 
                       'chave' => 'chave', 
                       'valor' => 'valor'
                    ]
                ];
    }
}
