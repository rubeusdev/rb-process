<?php
namespace Rubeus\FilaProcesso\Aplicacao;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\Bd\Persistencia;

abstract class Percorrer{
    static $processo;
    static $agendaProcesso;
    static $colocarRegistroFila;
    static $projeto;

    private static function carregarProcesso($fila){
        Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        $repositorio = Conteiner::get('RepositorioAgendaProcesso');
        self::$processo = $repositorio->consultar(self::$colocarRegistroFila, self::$projeto,$fila);
        Persistencia::mudarBase('principal');
    }
    
    public static function consultarProcesso($regsitro){
        Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        $repositorio = Conteiner::get('RepositorioAgendaProcesso');
        self::$processo = $repositorio->consultarProcessoFila($regsitro, self::$projeto);
        Persistencia::mudarBase('principal');
        return self::$processo;
    }
    
    private static function popularMensagem($msg,$parametro){
        for($i=0;$i<count($parametro);$i++){
            $msg->setCampo($parametro[$i]['chave'], $parametro[$i]['valor']);
        }
    }
    
    private static function excutarProcesso(){
        $msg = Conteiner::get('Mensagem');
        $repositorioProcesso = Conteiner::get('RepositorioProcesso');
        for($i=0;$i<count(self::$processo);$i++){
            var_dump(self::$processo[$i]['id'], '===============');
            $msg->setCampo('AgendaProcesso::id', self::$processo[$i]['id']);
            self::$agendaProcesso->setInicio(date('Y-m-d H:i:s'));
            
            self::popularMensagem($msg, self::$processo[$i]['paramentro']);
            $processo = $repositorioProcesso::get(self::$processo[$i]['processo'],self::$processo[$i]['etapa']);
            $resultado = $processo->executar($msg);
            
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
            $acaoPredefinidaProcesso = Conteiner::get('AcaoDefinidaProcesso');
            self::$agendaProcesso->setId(self::$processo[$i]['id']);
            self::$agendaProcesso->setExecutado(self::$processo[$i]['executado']+1);
            self::$agendaProcesso->setFim(date('Y-m-d H:i:s'));
            if($acaoPredefinidaProcesso->desativarAcao($msg, self::$processo[$i])){
                self::$agendaProcesso->setAtivo(0);
            }
            if(self::$colocarRegistroFila){
                self::$agendaProcesso->setFila(1);
            }
            self::$agendaProcesso->setResultado(intval($resultado['success']));
            self::$agendaProcesso->salvar();
            Persistencia::mudarBase('principal');
            $acaoPredefinidaProcesso->acaoFinal($msg,$processo,$resultado);
            self::$agendaProcesso->limparObjeto();
        }
    }
    
    public static function setProjeto($projeto){
        self::$projeto = $projeto;
    }
    
    public static  function run($id=false,$fila=false){        
        self::$colocarRegistroFila = $id;
        self::$agendaProcesso = ConteinerEntidade::getInstancia('AgendaProcesso');
        $i=0;
        //var_dump(self::$processo,'=###########################');
//        do{
            self::carregarProcesso($fila);
            if(self::$processo !== false){
                self::excutarProcesso();
            }
            $i++;
            Persistencia::commit();
//            sleep(10);
//        }while(TRUE);
    }
    
}
