<?php
namespace Rubeus\FilaProcesso\Aplicacao;

class Fila{
    private $processo;
    private $id;
    
    public function carregar(){
        
    }
    
    public function getIdElemento(){
        return $this->id;
    }
    
    public function registrar(){
        $registrou = 1;
        foreach($this->processo as $processo){
            $registrou *= intval($processo->registrar());
            $this->id[] = $processo->getAgendaProcesso()->getId();
        }
        return boolval($registrou);
    }
    
    public function add($processo,$chave=false){
        if(!$chave){
            $chave = $processo->getAgendaProcesso()->getProcesso().'/'.$processo->getAgendaProcesso()->getEtapa();
        }
        $this->processo[$chave] = $processo;
    }
    
    public function remove($chave){
      
    }
    
    public function get($chave){
      
    }
}