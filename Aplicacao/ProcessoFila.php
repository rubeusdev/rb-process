<?php
namespace Rubeus\FilaProcesso\Aplicacao;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\ContenerDependencia\Conteiner;

class ProcessoFila{
    private $agendaProcesso;
    private $parametroEtapa;
    
    public function criar($processo,$etapa, $fila=0,  $projeto=1) {
        $this->parametroEtapa = [];
        $this->agendaProcesso = ConteinerEntidade::getInstancia('AgendaProcesso');
        $this->agendaProcesso->setProcesso($processo);
        $this->agendaProcesso->setEtapa($etapa);
        $this->agendaProcesso->setMomento(Conteiner::get('DataHora',false)->get());
        $this->agendaProcesso->setAtivo(1);
        $this->agendaProcesso->setProjeto($projeto);
        $this->agendaProcesso->setFila($fila);
        $this->parametro = 0;
    }
    
    public function addParametro($chave,$valor,$tipo=1){
        $this->parametroEtapa[$this->parametro] = ConteinerEntidade::getInstancia('ParametroProcessoAgendado');
        $this->parametroEtapa[$this->parametro]->setChave($chave);
        $this->parametroEtapa[$this->parametro]->setValor($valor);
        $this->parametroEtapa[$this->parametro]->setTipoParametroProcessoAgendado($tipo);
        $this->parametroEtapa[$this->parametro]->setAgendaProcesso($this->agendaProcesso);
        $this->parametroEtapa[$this->parametro]->setMomento(Conteiner::get('DataHora',false)->get());
        $this->parametroEtapa[$this->parametro]->setAtivo(1);
        $this->parametro++;
    }
    
    public function recursivo($recursivo){
        $this->agendaProcesso->setRecursivo($recursivo);
    }
    
    public function intervaloExecucao($intervalo){
        $this->agendaProcesso->intervaloExecucao($intervalo);
    }
    
    public function getAgendaProcesso(){
        return $this->agendaProcesso;
    }
    
    public function registrar(){
        $registrou = 1;
        $registrou *= intval($this->agendaProcesso->salvar());
        foreach($this->parametroEtapa as $parametro){
            $registrou *= intval($parametro->salvar());
        }
        return boolval($registrou);
    }
}